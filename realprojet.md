# Réalisation du projet
## Installation du serveur FTP
Installation du serveur FTP "Proftpd"

`sudo apt-get install proftpd-basic`

Lors du processus d’installation, vous allez devoir choisir entre un serveur inetd ou standalone:
* inetd => le serveur ne se lancera que si une connexion FTP est initiée.
* standalone => le daemon tournera en permanence.

Je choisi standalone comme ça le serveur s'executera directement au démarrage de la machine

##  Ajout d’un utilisateur virtuel

On ne va pas ajouter un utilisateur système pour chaque personne ayant besoin d’accèder à ce serveur FTP,
il faut donc créer des utilisateurs virtuels. Accessoirement nous souhaitons que ces utilisateurs n’aient accès à aucun shell. 
Nous allons créer configurer un « faux shell » nommé /bin/false

Dans le fichier /etc/shells on ajoute l'entrée suivante

`/bin/false`

Une fois le "faux shell" créé on peut configurer notre utilisateur virtuel

`ftpasswd --passwd --name=mon-utilisateur --sha512 --uid=800 --gid=800 --file=/etc/proftpd/ftpd.passwd --home=/srv/ftp/ --shell=/bin/false`

Options utilisés:
* sha512 : nous permet d’utiliser l’algorithme sha512 à la place de md5
* uid & gid : il faut spécifier un user identifier et groupe identifier pour chaque utilisateur virtuel
* file : le fichier contenant les données utilisateurs
* home : le répertoire dans lequel notre utilisateur virtuel se connectera
* shell: le shell que notre utilisateur peut utiliser

## Génération d’un certificat SSL

Création du dossier qui contiendra notre certificat ainsi que la clé privée:

`sudo mkdir /etc/proftpd/ssl`

On entre dans le fichier créé:

`cd /etc/proftpd/ssl`

Et on y génére une clé privée un CSR et signons notre certificat:

`sudo openssl ecparam -genkey -name secp384r1 > ftp-key-p384.pem`

`sudo openssl req -new -key ftp-key-p384.pem -out csr.pem`

`sudo openssl req -x509 -days 3650 -key ftp-key-p384.pem -in csr.pem -out certificate.pem`

Maitenant protégeons ces fichiers:

`sudo chmod 400 ftp-key-p384.pem csr.pem`

`sudo chmod 600 certificate.pem`

## Configuration générale de protftpd
`sudo nano /etc/proftpd/proftpd.conf`

Pour empecher l'identification de la version de proftpd

* `ServerIdent      off` 

Pour donner un nom au serveur 

* `Servername       "FTPserver"`

Pour que le serveur puisse accepter les connexions des utilisateurs virtuels

* `AuthUserFile     /etc/proftpd/ftpd.passwd`

Pour que les utilisateurs ne puissent pas se balader partout sur le serveur
 
* `DefaultRoot      ~`

Modification du port d’écoute par défaut (21)

* `Port             2121`

Plage de ports utilisé pour le mode passif 

* `PassivePorts     30100 30200`

Pour configurer et sécuriser le protocole FTPS

* `Include          /etc/proftpd/tls.conf`

## Configuration SSL

* N’autoriser que les connexions sécurisées
* N’autoriser que les connexions via le protocole TLS 1.2
* Utiliser des ciphers sécurisés
* Utiliser le chiffrement implicite

Modification du fichier contenant la configuration du protocole FTPS

`sudo nano /etc/proftpd/tls.conf`

Contenu du fichier:

`<IfModule mod_tls.c>`

`TLSEngine on`

`TLSLog /var/log/proftpd/tls.log`

`TLSProtocol TLSv1.2`

`TLSCipherSuite EECDH+CHACHA20:EECDH+AESGCM:EECDH+AES;`

`TLSOptions UseImplicitSSL`

`TLSRSACertificateFile /etc/proftpd/ssl/certificate.pem`

`TLSRSACertificateKeyFile /etc/proftpd/ssl/ftp-key-p384.pem`

`TLSVerifyClient off`

`TLSRequired on`

`</IfModule>`

## Connection

On peut maintenant redémarrer le service proftpd et nous y connecter avec notre utilisateur

Pour redémarrer:

`sudo service proftpd restart`

Pour s'y connecter nous allons utilisé Filezilla

![](./Images/Conf-filezilla.PNG)

## Partage de dossiers
Création des dossiers dans lesquels nous monterons nos dossiers partagés

`cd /srv/ftp`

`sudo mkdir docs images videos`

Montage des dossier

`mount --bind /home/test1/images images`

`mount --bind /home/test1/videos videos`

`mount --bind /home/test1/docs docs`

Pour que ces partages soient permanents il vous faudra ajouter les entrées suivantes au fichier:

`/etc/fstab`

A noter qu’il faudra ajouter l’option _netdev pour chaque mount bind d’une ressource réseau

`## Partage FTP`

`mount --bind /home/test1/images images none 0 0`

`mount --bind /home/test1/videos videos none 0 0`

`mount --bind /home/test1/docs docs none 0 0`



