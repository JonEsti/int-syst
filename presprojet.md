# Presentation du projet
## Serveur FTP

FTP veut dire “File Transfert Protocol” ou Protocole de transfert de Fichier.
C’est donc un langage qui va permettre l’échange de fichiers entre 2 ordinateurs, et plus exactement entre un serveur et un client.
On parle alors de :

* serveur FTP
* client FTP

### Le serveur FTP

Le serveur FTP est un logiciel qui va répondre aux demandes des clients. Lorsque le serveur reçoit une demande, il vérifie les droits et si le client à les droits suffisants, il répond à cette demande sinon la demande est rejetée.
Le serveur FTP passe son temps à attendre. Si les demandes ne sont pas nombreuses, les ressources utilisées par le serveur FTP sont quasi-nulles.

### Le client FTP

C’est lui qui va être à l’initiative de toutes les transactions.
Il se connecte au serveur FTP, effectue les commandes (récupération ou dépôt de fichiers) puis se déconnecte. Toutes les commandes envoyées et toutes les réponses seront en mode texte. (cela veut dire qu’un humain peut facilement saisir les commandes et lire les réponses).
Le protocole FTP n’est pas sécurisé : les mots de passe sont envoyés sans cryptage entre le client FTP et le serveur FTP. (Le protocole FTPS avec S pour “secure” permet de crypter les données).

## A quoi ça sert FTP ?

Autrefois, il était incontournable d’utiliser FTP pour télécharger des fichiers. Maintenant, avec des connexions plus performantes, la plupart des téléchargement s’effectuent avec le navigateur web, en cliquant sur les liens proposés et les téléchargements démarrent directement. Pourtant dans certains cas encore, il est nécessaire d’utiliser FTP pour télécharger des fichiers.
Autant il est facile de télécharger des fichiers en surfant sur Internet, autant il serait difficile de mettre en ligne des fichiers sans le protocole FTP.
En effet, avec ce protocole, on va pouvoir se connecter aux différents serveurs et pouvoir y copier des fichiers (dans un sens ou dans un autre). Il est ainsi possible de sauvegarder ou d’envoyer des fichiers sur des serveurs distants sans passer par le web