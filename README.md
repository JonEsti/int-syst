# int-syst

## Projet
* [Présentation du projet](./presprojet.md)
* [Réalistaion du projet](./realprojet.md)

## Sécurisation du laptop
* [Doc Sécu Laptop](./séculaptop.md)

## Script backup base de donnée MySQL
* [Script BDD backup MySQL](./backupbddmysql.md)
