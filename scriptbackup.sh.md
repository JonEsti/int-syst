# Script de sauvegarde de la BDD MySQL

    #!/bin/bash

    #fichier de config
    source $HOME/.backupconf

    #nom de la base de donnée
    SQL=test

    #variable "date" 
    DATE=$(date +%Y-%m-%d)

    #backup de la bdd dans le fichier "Documents" le nom de la backup contient le nom de la bdd et la date.
    mysqldump -u$user -p$pass $SQL > /home/jon/Documents/$SQL"_"$DATE.sql
